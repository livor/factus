.. title: Cleto-vivencia
.. slug: invulnerables
.. date: 2019-01-16 23:00:00 UTC-03:00
.. link:
.. description:
.. tags: mathjax
.. author: klets




¿Cómo es ser ciclista?
---------------------------
Ser ciclista en la ciudad de México es como ser periodista de investigación, revelas verdades incómodas y eres perseguido y señalado por ello. Por ejemplo, eres capaz de ir más rápido en horas pico que el transporte público y bastante más eficiente que cualquier automovil hibrido o autónomo y a pesar de ello no se da espacio a esa verdad, se menosprecia o se desacredita.

Ser ciclista en la ciudad más antigua de América es como ser defensor de derechos humanos. Proveés un bienestar que se extiende más allá de la esfera individual y con resultados tanto inmediatos como a largo plazo pero a pesar de ello, o mejor dicho, a causa de ello te asesinan.

Elegir la bici como medio de transporte en una de las ciudades más densamente pobladas es como ser mujer en un ministerio público de México denunciando una violación sexual. Si ocurre un accidente se insiste en culpar a la víctima por haber provocado al abusador.

Ser ciclista en la ciudad de México es como ser un ciudadano crítico y congruente en uno de los países más corruptos y desiguales del mundo y que al hacer explicita su inconformidad de las injusticias políticas y económicas resulta vilipendiado por los mismos compatriotas que comparten su condición con adjetivos como "chairo", "rojillo", "grillero", "argüendero", "oportunista", "resentido" o con exclamaciones de todo tipo y que semióticamante no tienen más significado que el de abandonar la lucha; aunque muy en el fondo todos desearían que esa lucha fuese ganada por el indignado: "Ya ponte a trabajar", "Deja de soñar", "como la haces de a pedo", "El cambio sólo empieza en uno mismo".

Por muy imprudente que pueda ser un ciclista no deja de ser ese humano, que aún sin darse cuenta, es actor de cambio positivo para la sociedad. La opinión pública no generaliza a todo los automovilistas por un accidente cometido por un conductor. No hay justificación que sustente las opiniones negativas hacia los ciclistas, ¿cuántas muertes provocan al año los accidentes ciclistas? ¿cuántas provocadas por automovilistas? ¿cuánto estrés ahorra en el transporte público una persona que decide ocupar una bicicleta para ir al trabajo? ¿Cuánto impacta la producción y uso de un automóvil en los recursos del planeta? ¿cuánto impacta una bicicleta?

Los ciclistas, así como al defensor de derechos humanos o a los que luchan por la conservación y cuidado de los recursos naturales, deberían ser cuidados y protegidos por quienes gozan de su esfuerzo: la sociedad civil. La calidad de vida no está dada, todos los días alguien está haciendo algo para conseguirla: tus padres, tu pareja, algunos funcionarios públicos, tú. Todos los días hay una batalla contra el caos para que esto que llamamos realidad se mantenga a la escala de nuestras necesidades.

Que sociedad tan torpe debemos ser para no vislumbrar que no puede haber justicia cuando no hay las condiciones de equidad, está desproporcionado el cuidado y respeto que hay hacia los hombres comparado con las mujeres, entre automovilistas y ciclistas, urbanitas e indígenas.

No deja de ser paradójico esto y no soy el primero ni seré el último en señalarlo: lo valioso de este planeta lo estamos matando y nos estamos quedando con lo superficial, con la nata apestosa de este óceano de subjetividades y eso no vale nada para hacer mundo.
