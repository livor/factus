.. title: FakeOS
.. slug: fakeos
.. date: 2019-01-17 10:16:20 UTC-03:00
.. tags: fake, economia, etc
.. link: 
.. description: 
.. author: klets

Sistem operativo fallido
-------------------------

Los capitalistas fan boys, son como los geeks que defienden lo mucho que les encanta un sistema operativo o un dispositivo móvil. Todos sabemos, sin embargo, que aunque nos pudiese mucho gustar cualquier interfaz o diseño estos productos tienden a tener detrás efectos adversos, cómo vigilancia, recolección de datos, esclavitud fabril, costos aberrantes, aislamiento, sobreexplotación de recursos naturales. Así el capitalismo, es un sistema operativo que puede parecer eficiente, innovador, la mejor respuesta a la pregunta que nunca tuvimos, pero presenta grave fallas de origen, así como muy costosas externalidades negativas, es más caro que el supuesto bienestar que provee y tiende a mermar los recursos más potentes, paisaje, confianza, comunidad, ecosistemas diversos, disponibilidad de tiempo y energía y conforme avanza en ello se instalan con más peso atributos que nos hacen miopes-insensibles de estas afectaciones sólo para terminar absortos y encandilados con la ilusión de que ofrece bienestar, ¿cuándo? pronto, ¿por qué no ahora? porque aún no se ha hecho lo suficiente. 

Este sistema operativo económico hace que dejemos de sostener un vínculo con lo que nos proporciona vida y comenzamos a subestimar la potencia de habitar un planeta que genera riqueza por la simple innercia de su movimiento.

Citando a Bifo: "el capitalismo nunca había estado tan cerca de su colapso final, pero la solidaridad social nunca había estado tan lejos de nuestra experiecia diaria"

En varios de los experimentos que se han realizado investigando las respuestas que dan los humanos a disntitos dilemas sociales se ha observado una gran tendencia a la cooperación como si fuese algo innato. Somos un objeto extraño de estudio que no se adapta a la teoría económica, contradecimos los preceptos que dictan que en condiciones de competencia los humanos crean mayor valor. Y ya que no nos adaptamos a esos modelos, se presiona para generar las condiciones que produzcan humanos que se correspondan con la teoría: nos aislan de nuestros vínculos, se acota la comunicacion afectiva, todo lo que es del ámbito común se tiene en una constante campaña de cercamiento.

Este modeleo económico explota nuestra vulnerabilidad exhaltando la individualidad, el gozo a corto plazo, la acumulación sin consecuencias aparentes.

Máquinas más humanas y humanos más mecanizados. Ese es el futuro que anhela esta sistema operativo.

Sin embargo en nosotros habita esa cualidad especial de encontrar nuevas posibilidades en lo ya dado. Esta cualidad nos permitió en su momento sobreponernos a las circunstancias más desfavorables para nuestra especie. Sin garras ni colmillos grandes, o músculos y sentidos especiales logramos hacer habitable casi todas las latitudes de la Tierra.

Esa cualidad se despliega en lo colectivo, a través del Otro. Nuestra consciencia es una manifestación de una actividad social, requiere de una base fisiológica pero no habita en ella. Cuando estamos sólos incluso acudimos a una otredad para planear y realizar cosas "¿qué iba a escribir?", "uy, tengo hambre" "¿qué decisión debo tomar?"
Esa es nuestra habilidad más potente. Trabajar en conjunto, así inventamos la realidad a través de la organización de nuestras experiencias colectivas. Y sólo así vamos a reiventar las nuevas posibilidades que requerímos para dar de baja este torcido sistema operativo.


